package com.aspire.java8._1_streams._2_koans;

import java.math.BigDecimal;

public class Employee {

    private final String name;
    private final String surname;
    private final int age;
    private final BigDecimal salary;

    public Employee(String name, String surname, int age, BigDecimal salary) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public BigDecimal getSalary() {
        return salary;
    }
}
