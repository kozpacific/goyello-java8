package com.aspire.java8._1_streams._2_koans;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class StreamKoans {

    public static void main(String[] args) {
        new StreamKoans().exercise1();
    }

    // TODO: Utwórz stream skladajacy sie z ponizszych elementow, zamien go na liste
    public void exercise1() {
        String firstName = "Maciek";
        String secondName = "Anastazja";
        String thirdName = "Mikołaj";

    }

    // TODO: Zamień wszystkie litery na wielkie
    public void exercise2() {
        String firstName = "Maciek";
        String secondName = "Anastazja";
        String thirdName = "Mikołaj";

    }

    // TODO: Znajdz imiona zaczynajace sie na literę 'M'
    public void exercise3() {
        String firstName = "Maciek";
        String secondName = "Anastazja";
        String thirdName = "Mikołaj";

    }

    // TODO: Policz imiona zaczynajace sie na literę 'M'
    public void exercise4() {
        String firstName = "Maciek";
        String secondName = "Anastazja";
        String thirdName = "mikołaj";

    }

    // TODO: Wypisz wszystkie imiona pracowników
    public void exercise5() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Wypisz wszystkich pracowników którzy maja wiecej niz 38 lat
    public void exercise6() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Policz wszystkich pracowników zarabiajacych powyzej 5000 zł
    public void exercise7() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Posortuj pracowników według nazwiska
    public void exercise8() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Posortuj pracowników według imienia, a następnie według nazwiska
    public void exercise9() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Posortuj pracowników od najmłodszego do najstarszego
    public void exercise10() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Posortuj pracowników od najstarszego do najmłodszego
    public void exercise11() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Wypisz 3 pierwszych pracowników
    public void exercise12() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Wypisz 2 ostatnich pracowników
    public void exercise13() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Wypisz 3 najlepiej zarabiajacych pracowników
    public void exercise14() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Znajdź najmłodszego pracownika
    public void exercise15() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Sprawdź czy któryś z pracowników ma imię krótsze niż 5 znaków
    public void exercise16() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Sprawdź czy wszystkie nazwiska zaczynaja sie z wielkiej litery
    public void exercise17() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Czy wszyscy pracownicy zarabiaja powyzej 2000 zł?
    public void exercise18() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Upewnij się, że żaden z pracowników nie nazywa się John
    public void exercise19() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Ile znakow ma najdluzsze imie
    public void exercise21() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Znajdź najwieszy i najmniejszy wiek oraz policz sredni wiek
    public void exercise22() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Czy któreś z imion wśród pracowników się powtarza?
    public void exercise23() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Znajdz najdluzsze nazwisko
    public void exercise24() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Upewnij się, że nie istnieje dwoch pracownikow w takim samym wieku oraz o tym samym imieniu
    public void exercise25() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Stwórz mapę nazwisko -> pensja
    public void exercise26() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Stwórz mapę wiek -> pracownicy.
    public void exercise27() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Podziel pracowników na dwie grupy -> tych zarabiajacych powyzej 5000 zł i tych zarabiajacych mniej. Ktora grupa jest wieksza?
    public void exercise28() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Znajdz sredni wiek pracownika nie wykorzystujac IntStream
    public void exercise29() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Znajdz srednia, najmniejsza oraz najwieksza pensje pracownika korzystajac z metody reduce
    public void exercise30() {
        List<Employee> employees = getEmployees();

    }

    // TODO: Wypisz imiona i nazwiska pracowników w nastepujacym formacie:
    //       <imie1>-<nazwisko1>,<imie2>-<nazwisko2>
    public void exercise31() {
        List<Employee> employees = getEmployees();

    }

    private List<Employee> getEmployees() {
        return Arrays.asList(
                new Employee("Maciej", "Koziara", 26, BigDecimal.valueOf(2001)),
                new Employee("Mikołaj", "Koziara", 22, BigDecimal.valueOf(2142)),
                new Employee("Anastazja", "Lebedeva", 23, BigDecimal.valueOf(2020)),
                new Employee("Nikola", "Kucharska", 67, BigDecimal.valueOf(4767)),
                new Employee("Mikołaj", "Jankowski", 22, BigDecimal.valueOf(7756)),
                new Employee("Adam", "Wolski", 67, BigDecimal.valueOf(1976)),
                new Employee("Aleksandra", "Walczak", 47, BigDecimal.valueOf(8188)),
                new Employee("Bartek", "Kalinowski", 35, BigDecimal.valueOf(7444)),
                new Employee("Wiktoria", "Majewska", 35, BigDecimal.valueOf(3650)),
                new Employee("Kacper", "Dudek", 60, BigDecimal.valueOf(5690)),
                new Employee("Artur", "Czerwiński", 38, BigDecimal.valueOf(5287)),
                new Employee("Stanisław", "Kruk", 29, BigDecimal.valueOf(7331)),
                new Employee("Jan", "madej", 63, BigDecimal.valueOf(4223)),
                new Employee("Kacper", "Bednarek", 46, BigDecimal.valueOf(3800)),
                new Employee("Kalina ", "Kozłowska", 59, BigDecimal.valueOf(7764)),
                new Employee("Weronika", "Ostrowska", 58, BigDecimal.valueOf(6496)),
                new Employee("Piotr", "Adamski", 51, BigDecimal.valueOf(7022)),
                new Employee("Jeremiasz", "Jędrzejewski", 37, BigDecimal.valueOf(6730)),
                new Employee("Zuzanna", "Głowacka", 32, BigDecimal.valueOf(3221)),
                new Employee("Kinga", "Matusiak", 39, BigDecimal.valueOf(5998)),
                new Employee("Alicja", "Janiszewska", 44, BigDecimal.valueOf(2719)),
                new Employee("Michał", "Socha", 45, BigDecimal.valueOf(4595))
        );
    }
}
