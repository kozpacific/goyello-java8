package com.aspire.java8._1_streams._3_functions;

import java.util.Collections;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FunctionsExercise {

    // TODO: Nadaj typy poniższym funkcjom. Następnie wywołaj je.
    public static void main(String[] args) {
        FunctionsExercise exercise = new FunctionsExercise();

        String stringResult = exercise.getString();
        System.out.println(stringResult);

    }

    public String getString() {
        return "Just return string";
    }

    public void doSomethingWithString(String stringToReverse) {
        String reversed = new StringBuilder(stringToReverse).reverse().toString();
        System.out.println("Reversed string: " + reversed);
    }

    public String convert(Integer integer) {
        return String.valueOf(integer);
    }

    public boolean moreThatZero(Integer number) {
        return number > 0;
    }

    public void doNothing() {
        System.out.println("Just do nothing");
    }

    public Character charAt(String string, Integer index) {
        return string.charAt(index);
    }

    public String upperCase(String string) {
        return string.toUpperCase();
    }

    public String  complexOperation(String sentence, Integer factor, Character separator) {
        return Stream.generate(() -> sentence)
                .limit(factor)
                .collect(Collectors.joining(sentence));
    }
}