package com.aspire.java8._2_optional._2_koans;

import java.util.Arrays;
import java.util.List;

public class Student {

    private final String name;
    private final List<Integer> grades;
    private final boolean hasDebt;

    private Student(String name, List<Integer> grades, boolean hasDebt) {
        this.name = name;
        this.grades = grades;
        this.hasDebt = hasDebt;
    }

    public static Student noDebt(String name, Integer... grades) {
        return new Student(name, Arrays.asList(grades), false);
    }

    public static Student inDebt(String name, Integer... grades) {
        return new Student(name, Arrays.asList(grades), true);
    }

    public String getName() {
        return name;
    }

    public List<Integer> getGrades() {
        return grades;
    }

    public boolean isInDebt() {
        return hasDebt;
    }
}
