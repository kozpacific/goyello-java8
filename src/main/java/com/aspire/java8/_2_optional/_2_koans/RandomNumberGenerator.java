package com.aspire.java8._2_optional._2_koans;

import java.util.Optional;
import java.util.Random;

public class RandomNumberGenerator {

    public static final Random RANDOM = new Random();

    public Optional<Integer> generateNullSafeNumber() {
        int number = RANDOM.nextInt(10000);
        return null;
    }

    public Integer generateNumber() {
        int number = RANDOM.nextInt(10000);
        if (number > 5000) {
            return number;
        }

        return null;
    }
}
