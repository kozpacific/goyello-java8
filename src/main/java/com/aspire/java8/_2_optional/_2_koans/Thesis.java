package com.aspire.java8._2_optional._2_koans;

public class Thesis {

    private final String topic;
    private final Student student;

    private Thesis(String topic, Student student) {
        this.topic = topic;
        this.student = student;
    }

    public static Thesis assigned(String topic, Student student) {
        return new Thesis(topic, student);
    }

    public static Thesis notAssigned(String topic) {
        return new Thesis(topic, null);
    }

    public String getTopic() {
        return topic;
    }

    public Student getStudent() {
        return student;
    }
}
