package com.aspire.java8._2_optional._2_koans;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.aspire.java8._2_optional._2_koans.Student.inDebt;
import static com.aspire.java8._2_optional._2_koans.Student.noDebt;

public class OptionalKoans {

    public static void main(String[] args) {

    }

    // TODO: Zaimplementuj metodę generateNullSafeNumber w klasie RandomNumberGenerator
    //       Jeżeli wylosowana liczba jest parzysta powinna liczba powinna zostać zwrócona, jeżeli jest nieparzysta powinien zostać zwrócony
    //       pusty optional. Wypisz na ekran informację czy liczba była parzysta
    public void exercise1() {
        RandomNumberGenerator randomNumberGenerator = new RandomNumberGenerator();

    }

    // TODO: Wywołaj metodę generate number i wypisz na ekran wylosowana liczbę. Zabezpiecz się przed NPE
    public void exercise2() {
        RandomNumberGenerator randomNumberGenerator = new RandomNumberGenerator();

    }

    // TODO: Znajdź pierwszego studenta który ma średnią ocen powyżej 4.0. Wypisz jego imię wielkimi literami i dodaj na końcu wykrzyknik
    //       Jeżeli taki student nie istnieje rowniez wypisz stosowna informację
    public void exercise3() {
        List<Student> students = getStudents();
    }

    // TODO: Spośród studentów których imię zaczyna się na literę 'J' znajdź tego z najmniejszą średni. Wypisz jego imię jeżeli nie posiada on długu,
    //       w przeciwnym wypadku rzuć stosowny wyjątek
    public void exercise4() {
        List<Student> students = getStudents();
    }

    // TODO: Wypisz wszystkie tematy które nie posiadają przypisanych studentów
    public void exercise5() {
        List<Thesis> theses = getTheses();
    }

    // TODO: Znajdź imię studenta tematu o nazwie 'Extra topic'
    public void exercise6() {
        List<Thesis> theses = getTheses();
    }

    private List<Thesis> getTheses() {
        return Arrays.asList(
                Thesis.assigned("Fun topic", inDebt("Jack")),
                Thesis.notAssigned("Boring topic"),
                Thesis.assigned("Extra topic", noDebt("Maciek")),
                Thesis.notAssigned("Toxic topic"),
                Thesis.notAssigned("Dangerous topic"),
                Thesis.assigned("Little topic", inDebt("Jill"))
        );
    }

    private List<Student> getStudents() {
        return Arrays.asList(
                inDebt("Mikołaj", 3, 2, 3, 3, 4),
                noDebt("Anastazja", 4, 4),
                noDebt("Maciek", 5, 5, 4, 3),
                noDebt("John", 5, 2, 1, 4),
                inDebt("Jack", 3, 3, 2, 3),
                noDebt("Jill", 1, 1, 2, 3),
                inDebt("James", 2, 5, 3, 1, 4, 5, 6)
        );
    }
}