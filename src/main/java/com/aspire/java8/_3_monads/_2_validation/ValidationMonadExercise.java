package com.aspire.java8._3_monads._2_validation;

public class ValidationMonadExercise {

    // TODO: Napisz klasę enkapsulującą wynik walidacji, tak aby można się nia posługiwać zamiast standardowego booleana.
    //       Klasa powinna nazywać się ValidationResult, być generyczna oraz oferować następujące funkcjonalności:
    //       Metody tworzące:
    //        * ValidationResult.valid(object) -> statyczna metoda pozwalajaca na utworzenie obiektu ktory przeszedł poprawnie walidację
    //        * ValidationResult.invalid() -> statyczna metoda pozwalajaca na utworzenie obiektu ktory nie jest poprawny
    //        * ValidationResult.from(object, predicate) -> statyczna metoda pozwalajaca na przekazanie obiektu do sprawdzenia
    //          oraz predykatu, który okresli czy obiekt jest poprawny czy nie
    //       Metody operująna zwalidowanym obiekcie:
    //        * and(predicate), or(predicate) -> dodatkowe predykaty określający czy obiekt jest lub nie poprawny
    //        * filter(predicate) -> pozwala na przekazanie warunku, który powinien spełniać zwalidowany obiekt
    //        * map(function) -> pozwala na przekształcenie zwalidowanego obiektu w inny
    //       Metody wyciągające zwalidowaną wartość:
    //        * get() -> wymusza zwrócenie zwalidowanego obiektu. Jeżeli obiekt nie był poprawny rzuć wyjątek
    //        * getIfInvalid(object) -> zwraca zwalidowany obiekt jezeli byl poprawny, jezeli nie zwraca ten przekazany do metody
    //        * getIfInvalid(function) -> wariacja na temat metody wyżej. Zasada działania jak wyżej tylko przekazujemy metodę,
    //          która zwróci nam obiekt
    //        * throwIfInvalid(exception) -> zwróc poprawny obiekt lub rzuć wyjątek jeżeli obiekt nie był poprawny
    //        * doIfValid(function) -> zrób coś jeżeli obiekt był poprawny
    //        * doIfValidOrElse(function, function) -> wykonaj pierwsza funkcje jezeli obiekt był poprawny lub wykonaj drugą jeżeli obiekt nie był poprawny
    public static void main(String[] args) {
        System.out.println("######## Simple ########");
        simpleProcessing("zle");
        simpleProcessing("Troche lepsze.");

//        System.out.println("######## Fallback ########");
//        fallbackProcessing("zle zdanie.");
//        fallbackProcessing("Jeszcze lepsze zdanie.");
//
//        System.out.println("######## Throwing ########");
//        throwingProcessing("Gorsze zdanie");
//        throwingProcessing("Jeszcze nieco lepsze zdanie.");
//
//        System.out.println("######## Complex ########");
//        complexProcessing("skomplikowane");
//        complexProcessing("Dluzsze oraz nieco lepsze zdanie.");
    }

    private static void simpleProcessing(String sentence) {
        StringLengthValidator stringLengthValidator = new StringLengthValidator();
        if (stringLengthValidator.validate(sentence)) {
            System.out.println(sentence.toUpperCase());
        }
    }

    private static void fallbackProcessing(String sentence) {
        StringLengthValidator stringLengthValidator = new StringLengthValidator();
        if (stringLengthValidator.validate(sentence)) {
            System.out.println("This senstence is valid: " + sentence.toUpperCase());
        } else {
            System.out.println("Sentence was invalid");
        }
    }

    private static void throwingProcessing(String sentence) {
        StringLengthValidator stringLengthValidator = new StringLengthValidator();
        if (stringLengthValidator.validate(sentence)) {
            System.out.println("Valid sentence: " + sentence.toUpperCase());
        } else {
            throw new IllegalArgumentException("Invalid sentence!");
        }
    }

    private static void complexProcessing(String sentence) {
        StringLengthValidator stringLengthValidator = new StringLengthValidator();
        if (stringLengthValidator.validate(sentence) && sentence.split(" ").length > 3) {
            String word = sentence.split(" ")[3];
            Character c = word.toUpperCase().charAt(0);
            System.out.println(c);

        } else {
            throw new IllegalArgumentException("Zdanie niepoprawne lub za krótkie!");
        }
    }
}
