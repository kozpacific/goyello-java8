package com.aspire.java8._3_monads._2_validation;

public class StringLengthValidator {

    public boolean validate(String sentence) {
        if (sentence.isEmpty()) {
            return false;
        }

        return Character.isUpperCase(sentence.charAt(0))
                && sentence.endsWith(".")
                && sentence.contains(" ");
    }
}