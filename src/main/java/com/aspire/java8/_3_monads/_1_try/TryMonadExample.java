package com.aspire.java8._3_monads._1_try;

public class TryMonadExample {

    // TODO: Napisz klasę enkapsulującą łapanie wyjątków. Nazwij tę klasę Try i zaimplementuj następujące metody:
    //       * failable(function) -> metoda tworząca która przyjmuje jako argument metodę która może rzucić wyjątek.
    //       * isSuccess() -> zwraca informację czy wykonanie metody zakończyło się sukcesem
    //       * onSuccess(function) -> przyjmuje funkcję która wykona się gdy wyjątek nie został rzucony
    //       * catch(function) -> przyjmuje funkcję która zostanie wykonana gdy wyjątek został rzucony
    //       * catch(Exception.class, function) -> przyjmuje funkcję która powinna zostać wykonana tylko gdy rzucony został wyjątek konkretnej klasy
    //       * get() -> zwraca wartość gdy sukces, rzuca ponownie wyjątek gdy failure
    //       * recover(object) -> wartość która powinna zostać zwrócona gdy wyjątek został rzucony
    public static void main(String[] args) {

    }

}
