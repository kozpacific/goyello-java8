package com.aspire.java8._4_processing;

public class EmployeeDataProcessor {

    // TODO: Napisz aplikację umożliwiającą wczytywanie danych pracowników z różnych źródeł, procesowanie ich a następnie zapisywanie w inne miejsce.
    //       Na początek aplikacja powinna pozwalać na odczytanie danych z pliku csv, który można znaleźć w folderze 'data' oraz bazy danych którą można utworzyć
    //       z pliku sql również znajdującego się w folderze 'data'. Zarówno ścieżka do pliku jak i parametry do połączenia z bazą danych powinny być konfigurowalne
    //       przez końcowego użytkownika aplikacji.
    //       Po wczytaniu danych ze wszystkich źródeł aplikacja:
    //        * policzy srednia pensje pracowników posiadających ten sam nr dowodu
    //        * upewni się, że imiona i nazwiska pracowników z tym samym nr dowodu są identyczne. Jeżeli nie będa wskaże ile jest błędów i które litery się nie zgadzaja (*).
    //       Wynik działania programu powinien zostać zapisany do pliku csv (wskazanego przez użytkownika) w formacie:
    //       <nr-dowodu>;<srednia-pensja>;<ilosc-bledow>;<bledy-w-imieniu-i-nazwisku-rozdzielone-znakiem-|>
    //       np.
    //       ABC12345;2080.5;2;'A'<>'B'|'t'<>'r'
    public static void main(String[] args) {

    }
}