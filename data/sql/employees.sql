CREATE TABLE employee
(
  id        INT AUTO_INCREMENT,
  name      TEXT,
  id_number INT,
  SALARY    INT
);

insert into employee (name, id_number, SALARY)
values
  ('John', 47524, 8891),
  ('Daniel', 84361, 22384),
  ('Heather', 93808, 10836),
  ('Joshua', 18213, 6487),
  ('Jessica', 10939, 8210),
  ('Nicole', 63081, 16480),
  ('James', 69446, 24696),
  ('Matthew', 43628, 20178),
  ('Christopher', 66332, 5284),
  ('Amanda', 30724, 14271),
  ('Stephanie', 73252, 8724),
  ('Michelle', 27386, 16795),
  ('Melissa', 68348, 9383),
  ('Michael', 70563, 10160),
  ('David', 80149, 15723),
  ('Jennifer', 23594, 9047),
  ('Robert', 69533, 6843),
  ('Sarah', 93126, 5120),
  ('ymA', 6374, 11608),
  ('Laura', 27315, 18256),
  ('Jason', 44734, 8755),
  ('Ryan', 52321, 21191),
  ('kevin', 51483, 14526),
  ('Rebegga', 17665, 10884),
  ('Kelly', 95487, 16137),
  ('Brian', 24735, 8122),
  ('Angela', 90876, 13110),
  ('Joseph', 81661, 9531),
  ('Elizabeth', 43959, 23058),
  ('Elizabeth', 64402, 17072);